module HNNLS
export update_H!, select_solution_of_size, select_in_budget

using LinearAlgebra: norm
using JuMP, GLPK



include("../src/Homotopy.jl")


"""
    update_H!(M, W, H; lambda, selection_function)

Given M a matrix of size m*n and W a matrix of size m*r and select_function a function
, return a matrix H of size r*n such that M =~ W * H. In order to compute H,
we find the possible supports for every column of H. Then the best support is
selected by the selection_function
"""
function update_H!(M, W, H; lambda=0, selection_function)
    possible_solutions = []
    for j in 1:size(M, 2)
        push!(possible_solutions, Homotopy.find_breakpoints(W, M[:, j]))
    end
    H .= selection_function(M, W, possible_solutions)
end #function

"""
    select_solution_of_size(M, W, possible_solutions, k)

Given M a matrix of size m*n and W a matrix of size m*r, an integer k and
possible_solutions a list of length n that contains some lists l. Each list l
contains some tuples composed by a matrix x of size r*1 and a number k'.
This function returns a matrix H of size r*n composed by every best x element
of each list l such that k' ~= k.
"""
function select_solution_of_size(M, W, possible_solutions, k)
    x = zeros(size(W, 2), size(M, 2))
    for i in 1:length(possible_solutions)
        column_solutions = possible_solutions[i]
        new_col = find_best_support(M[:, i], W, column_solutions, k)
        x[:, i] .= new_col[:, 1]
    end # for
    return x
end # function

"""
    find_best_support(M_i, W, column_solutions, k)

Given M_i a matrix of size m*1, W a matrix of size m*r, columns_solutions
a list that contains some tuples composed by a matrix x of size r*1
and a interger k, return the best x which the associated k' is equal (or near) to k. The
best x is the one minimize ||W*x - M_i||_2.
"""
function find_best_support(M_i, W, column_solutions, k)
    best_x, best_size = column_solutions[1]
    best_error = reconstruction_error(W, best_x, M_i)
    for i in 1:length(column_solutions)
        x, size = column_solutions[i]
        # Check if the the new size is nearer to k than best_size
        # This is useful if there is no size equal to k, in this case we pick the
        # best x among the ones which size is the nearest to k
        if abs(best_size - k) == abs(size - k)
            current_error = reconstruction_error(W, x, M_i)
            if best_error > current_error
                best_x = x
                best_error = current_error
                best_size = size
            end # if
        elseif abs(best_size - k) > abs(size - k)
            best_x = x
            best_error = reconstruction_error(W, x, M_i)
            best_size = size
        end # if
    end #for
    return best_x
end # function

"""
    select_in_budget(M, W, possible_solutions, budget; limit=100, use_limit=true)

Given M a matrix of size m*n and W a matrix of size m*r, an integer budget,
limit: a time in seconds, a boolean use_limit and possible_solutions a list of length n that contains
some lists l. Each list l contains some tuples composed by a matrix x of size r*1
and a number k'. This function build H a the best matrix of size r*n such that
M ~= W * H and the total number of non-zero values of H is lesser or equal to budget.
This is done in maximum "limit" seconds if use_limit is true, there is no limit of time.
"""
function select_in_budget(M, W, possible_solutions, budget; limit=100, use_limit=true)
    C, index_matrix = create_C_matrix(M, W, possible_solutions)
    supports = compute_optimal_supports(C, budget, limit, use_limit)
    return retrieve_H(supports, index_matrix, possible_solutions)
end # function

"""
    retrieve_H(supports, index_matrix, possible_solutions)

Given "supports" a matrix of size r+1*n that indicates the size of the support to
use to build each column of H, index_matrix a matrix of size r+1*n that indicates
where to find the supports represented by "supports"  and possible_solutions a list
of length n that contains some lists l. Each list l contains some tuples composed
by a matrix x of size r*1 and a number k. This function build H by using the supports
selected by "supports" then retrieve them in possible_solutions thanks to index_matrix.
"""
function retrieve_H(supports, index_matrix, possible_solutions)
    H = zeros(size(supports, 1)-1, size(supports, 2))
    for j in 1:size(supports, 2)
        for i in 1:size(supports, 1)
            if supports[i, j] == 1.0
                if i==1
                    H[:, j] .= zeros(size(supports, 1)-1, 1)[:, 1]
                else
                    support_index = Int(index_matrix[i, j])
                    x, support_size = possible_solutions[j][support_index]
                    H[:, j] .= x[:, 1]
                end # if
                break
            end # if
        end # for
    end # for
    return H
end # function

"""
    compute_optimal_supports(C, budget, limit, use_limit)

Given C a matrix of cost of size r+1*n, budget an integer and limit a time in seconds,
find the optimal supports x: a matrix of size r+1*n which x[i, j] is 1 if the
(i-1)-sparse support of the j-th column is in the optimal solution, otherwise it
is 0. x is computed so that 1 and only 1 support is selected for each column,
the sum of (i-1)*x[i, j] is lower or eaqual to budget and the sum error for each
selected support is minimized. If use_limit is true, return the best solution found
within the limit time, the best solution found is returned. Otherwise, the optimal
solution is returned with no limit of time.
"""
function compute_optimal_supports(C, budget, limit, use_limit)
    model = Model(GLPK.Optimizer)
    if use_limit
        set_time_limit_sec(model, limit)
    end # if
    # Create the matrix variable x of size r+1*n where x[i, j] is in {0, 1} (binary)
    # for all i, j.
    @variable(model, x[i=1:size(C, 1), j=1:size(C, 2)], Bin)
    # Create the constraint that ensure only one support is selected for each column
    for j in 1:size(C, 2)
        @constraint(model, sum(x[i, j] for i in 1:size(C, 1)) == 1)
    end # for
    # Create constraint to ensure the total number of non-zero values (in H) is lower
    # or equal to the budget
    @constraint(model, sum((i-1) * sum(x[i, j] for j in 1:size(C, 2)) for i in 1:size(C, 1)) <= budget)
    # We want to minimize the total error induce by the support selected by x
    @objective(model, Min, sum(sum(x[i, j] * C[i, j] for j in 1:size(C, 2)) for i in 1:size(C, 1)))
    optimize!(model)
    return value.(x)
end # function

"""
    create_C_matrix(M, W, possible_solutions)

Given M a matrix of size m*n, W a matrix of size m*r and possible_solutions a list
of length n that contains some lists l. Each list l contains some tuples composed
by a matrix x of size r*1 and a integer k such that 0 < k <= r. This function
compute a matrix C of size r+1 * n such that each column represents the lists l and
each line represents the integers k (the first line represents k=0). Each element
is thus the error corresponding to the line and column: C[i, j] = ||Wx - M[:, j]||_2
where x is the matrix which k = i-1 of the j-th list of possible_solutions.
The matric C is a matrix of cost. It also computes an index_matrix of size r+1*n
that tracks the different values used to build C in order to retrieve them faster.
"""
function create_C_matrix(M, W, possible_solutions)
    C = ones(size(W, 2)+1, size(M, 2))
    index_matrix = zeros(size(C))
    for j in 1:size(C, 2)
        column_solutions = possible_solutions[j]
        for i in 1: length(column_solutions)
            x, support_size = column_solutions[i]
            error = reconstruction_error(W, x, M[:, j])
            if error < C[support_size+1, j]
                C[support_size+1, j] = error
                index_matrix[support_size+1, j] = i
            end # if
        end # for
    end # for
    return C, index_matrix
end # function

"""
    reconstruction_error(A, x, b)

Compute the reconstruction error: ||Ax - b||_2
"""
function reconstruction_error(A, x, b)
    return norm(b - A * x) / norm(b)
end # function

end #module
