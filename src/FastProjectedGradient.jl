module FPG
using LinearAlgebra
export update_H!, update_W!, init_matrices

"""
    init_matrices(M, W)

Given a matrix M of size m*n and an integer r, returns 2 randomly generated
matrices: W of size m*r et H of size r*n. W is then scaled.
"""
function init_matrices(M, r; base_init=NMFUtils.random_initialization)
    W, H = base_init(M, r)
    scale_W!(M, W, H)
    return W, H
end # function

"""
    scale_W!(M, W, H)

Replace W by a good approximation of it and then scale it.
"""
function scale_W!(M, W, H)
    scale_H!(M', H', W')
end # function

"""
    scale_H!(M, W, H)

Replace H by a good approximation of it and then scale it.
"""
function scale_H!(M, W, H)
    H = max.(0, W \ M)
    alpha = sum(sum( H .* (W' * M) )) / sum(sum( (W' * W) .* (H * H')))
    H .= H .* alpha
    for row in 1:size(H, 1)
        if sum(H[row, :]) == 0
            H[row, :] = 0.001 * maximum(H) * rand(1, size(H,2))
        end # if
    end
end # function

"""
update W by col
"""
function update_W!(M, W, H; alpha0=0.05, num_iter=100, delta=1e-6, lambda)
    update_H!(M', H', W', alpha0=alpha0, num_iter=num_iter, delta=delta, lambda=lambda)
end

"""
    update_H!(M, W, H; alpha0=0.05, num_iter=500, delta=1e-6, lambda)

Update W column-wise thanks to a fast projected gradient algorithm.
"""
function update_H!(M, W, H; alpha0=0.05, num_iter=5, delta=1e-6, lambda)
    #display(H)
    WtW = W'*W
    WtM = W'*M
    old_alpha = alpha0
    new_alpha = 0
    beta = 0
    simplex_col_proj!(H')
    L = norm(WtW)
    Y = copy(H)
    i=1
    eps0, eps = 0, 1
    while i <= num_iter && eps >= delta*eps0
        old_H = copy(H)
        #println("old: $old_alpha, new: $new_alpha")
        new_alpha = ( sqrt(old_alpha^4 + 4*old_alpha^2 ) - old_alpha^2) / (2)
        beta = old_alpha*(1-old_alpha)/(old_alpha^2+new_alpha)
        old_alpha = new_alpha
        # Projection step
        #display(Y - (WtW*Y-WtM) ./ L)
        H .= Y - (WtW*Y-WtM) ./ L
        simplex_col_proj!(H') # Project rows of H onto the simplex
        # `Optimal' linear combination of iterates
        Y .= H + beta * (H - old_H)
        if i == 1
            eps0 = norm(H - old_H)
        end
        eps = norm(H - old_H)
        i += 1
    end # while
end # function

"""
    simplex_col_proj!(W; epsilon=0)

This function projects each column x of a matrix X on the set defined by
 {y in R^d s.t. y>=-epsilon, sum_i y_i=1}
The solution is given, column-wise, by y_i=max(-epsilon, x_i-mu) for all
i, where mu is given by solving sum_i max(-epsilon, x_i-mu)=1

****** Input ******
X       : d-by-r matrix
epsilon : sclara or r-by-1 vector, generally positive
r       : factorization rank r

****** Output ******
Y       : the projected matrix

Code from the paper
P. De Handschutter, N. Gillis, A. Vandaele and X. Siebert,
"Near-Convex Archetypal Analysis", IEEE Signal Processing Letters 27 (1),
pp. 81-85, 2020.

The original code was written by Nicolas Gillis and then slightly modified.
"""
function simplex_col_proj!(H; epsilon=0)
    if length(epsilon) == 1
        epsilon_matrix = epsilon * ones(size(H, 2), 1)
    else
        epsilon_matrix = epsilon
    end
    for i in 1:size(H, 2)
        # sort each column of the input matrix
        x = H[:,i]
        x_bis = deepcopy(sort(x))

        len = length(x)

        #= These lines are not used in the code
        # mu s.t. y_i < mu-epsilon_matrix, forall i
        mu_max = x_bis[len] + epsilon_matrix[i]
        =#

        # mu s.t. y_i > mu-epsilon_matrix, forall i
        mu_min = x_bis[1]  + epsilon_matrix[i]

        somme_min = sum(x) - len * mu_min
        somme_max = -len * epsilon_matrix

        if (somme_min < 1)
            mu = (sum(x)-1)/len
            H[:,i] = max.(-epsilon_matrix[i] .* ones(len, 1), x .- mu)
        else
            index_min , index_max = find_optimal_boundaries!(1, len, i, epsilon_matrix, x, x_bis, H)
            mu_opt = compute_optimal_mu(index_min, index_max, i, epsilon_matrix, x, x_bis)

            # compute the corresponding column of H
            H[:,i] = max.(-epsilon_matrix[i] .* ones(len, 1), x .- mu_opt)
        end #condition

    end # for
end # function

"""
    find_optimal_boundaries!(index_min, index_max, current_col, epsilon, x, x_bis, W)

Find the boundaries in order to compute the optimal mu. The 'current_col' of W
is updated durieng the process.
"""
function find_optimal_boundaries!(index_min, index_max, current_col, epsilon, x, x_bis, H)
    # apply a dichotomy to find the optimal value of mu
    stop = false
    len = length(x)
    while !stop
        curr_ind = Int(round((index_min+index_max)/2))
        mu = x_bis[curr_ind] + epsilon[current_col]
        h = max.(-epsilon[current_col] .* ones(len, 1), x .- mu)
        somme = sum(h)
        if (somme < 1)
            index_max = curr_ind
        elseif (somme > 1)
            index_min = curr_ind
        else
           H[:,current_col] = h
           stop = true
        end
        if index_max == index_min +1
            stop = true
        end
    end # while
    return index_min, index_max
end

"""
    compute_optimal_mu(index_min, index_max, current_col, epsilon, x, x_bis)

Compute the optimal mu based on the optimal boundaries.
"""
function compute_optimal_mu(index_min, index_max, current_col, epsilon, x, x_bis)
    len = length(x)
    mu_inf = x_bis[index_min] + epsilon[current_col]
    mu_sup = x_bis[index_max] + epsilon[current_col]
    obj_inf = sum(max.(-epsilon[current_col] .* ones(len, 1), x .- mu_inf))
    obj_sup = sum(max.(-epsilon[current_col] .* ones(len, 1), x .- mu_sup))
    # avoid a zero division to compute the slope
    if mu_sup == mu_inf
        return 0
    else
        slope = (obj_sup-obj_inf)/(mu_sup - mu_inf)
        mu_opt = (1-obj_inf)/slope + mu_inf
        return mu_opt
    end # if
end # function

end  # module FPG
