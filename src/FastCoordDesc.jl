module FastCoordDesc
using LinearAlgebra: diag
export update_H!

# Ci-dessous, le code de N², sûrement à revoir ################################
"""
    update_H!

Update H by running column-wise NNLS with simple coordinate descent
NMF:   M      = W H
by col M(:,j) = W H(:,j)
NNLS:  b      = A x
"""
function update_H!(M, W, H; lambda)
    ata = W' * W
    for j in 1:size(H, 2)
        atb = W' * M[1:end, j]
        #H[:,j] = simple_coorddesc(ata, atb, H[:,j], lambda=lambda)
        H[:,j] = fast_coorddesc(ata, atb, H[:,j], lambda=lambda[j])
    end
end

"""
    fast_coorddesc

Update given column-vector x (r*1) such that min_x Ax-b, with matrix A (m*r) and
column-vector b (m*1).
Adapted from the paper by Hsieh and Dhillon, "Fast coordinate descent methods with
variable selection for non-negative matrix factorization", 2011.
"""
function fast_coorddesc(ata, atb, x; lambda=0, maxiter=5)
    # Init
    r = size(x, 1)
    diagata = diag(ata)
    initdiffobj = 1.0

    # Compute gradient
    g = ata * x - atb
    # L1 penalty
    g .+= lambda * sum(x)

    # NNLS loop
    for iter in 1:r*maxiter
        # Select iopt, the entry we will update to maximize the difference in the objective
        iopt = 1
        maxdiffobj = 0.0
        for i in 1:r
            d = g[i] / diagata[i]
            tmpxi = x[i] - d
            if tmpxi < 0
                tmpxi = 0
                d = x[i]
            end
            diffobj = d*g[i] - 0.5*diagata[i]*d*d
            if diffobj > maxdiffobj
                maxdiffobj = diffobj
                iopt = i
            end
        end
        # If first iteraion, set initdiffobj
        iter == 1 && (initdiffobj = maxdiffobj)

        # Update the iopt-th entry
        oldxiopt = x[iopt]
        d = g[iopt] / diagata[iopt]
        x[iopt] -= d
        if x[iopt] < 0
            x[iopt] = 0
            d = oldxiopt
        end

        # Update gradient
        for j in 1:r
            g[j] -= ata[iopt, j] .* d
        end
        g .+= lambda * sum(x)

        # Stopping criterion
        maxdiffobj < 0.01 * initdiffobj && break
    end
    return x
end

"""
    simple_coorddesc

Update given column-vector x (r*1) such that min_x Ax-b, with matrix A (m*r) and
column-vector b (m*1).
"""
function simple_coorddesc(ata, atb, x; lambda=0, maxiter=100)
    # Init
    r = size(x, 1)
    diagata = diag(ata)

    # Compute gradient
    g = ata * x - atb

    # NNLS loop
    for iter in 1:maxiter
        # Loop on all entries of x
        for i in 1:r
            oldxi = x[i]
            d = (g[i] .+ lambda * sum(x)) / diagata[i]
            x[i] -= d
            # Nonnegativity
            if x[i] < 0
                x[i] = 0
                d = oldxi
            end
            # Update gradient
            for j in 1:r
                g[j] -= ata[j, i] * d
            end
        end
    end
    return x
end

end # end of module NMF
