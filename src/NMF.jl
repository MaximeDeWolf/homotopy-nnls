module NMF
export nmf

"""
    nmf(M, r; update_W!, update_H!, scale_func!, initialization=NMFUtils.random_initialization, max_iter=100, lambda=0)

Given a matrix M of size m*n, and a positive int r, returns two matrices W and H
of respective size m*r and r*n. These matrices are computed using the 'update_W!'
and 'update_H!' functions.

An initialization function can be provided to process the initialization of the
matrices W and H. By default, this function is 'random_initialization'.
The 'max_iter' parameter can be used to specify to number of update of the
matrices W and H. Its default value is 100. The lambda is either a matrix of size
r*1 which allows to control the L1 regularization or a number that will be used
to create a r*1 matrix full of 'lambda'. If lambda is not provided, a matrix full
of 0 of size r*1 is used instead. The scale_func allows to apply a scaling on the
W and H matrices. By default, no scaling is applied.
"""
function nmf(M, r; update_W!, update_H!, scale_func! =(m, w, h)->return, initialization=random_initialization, max_iter=100, lambda=0)
    lambda_matrixH = initlambda(r, lambda)
    lambda_matrixW = initlambda(r, 0)
    W, H = initialization(M, r)
    scale_func!(M, W, H)
    for i in 1:max_iter
        update_W!(M, W, H, lambda=lambda_matrixW)
        #Update H by row
        update_H!(M, W, H, lambda=lambda_matrixH)
    end # for
    return W, H
end # function

"""
    random_initialization(M, r)

Given a matrix M of size m*n, and a positive int r, returns two random matrices
W and H of respective size m*r and r*n.
"""
function random_initialization(M, r)
    m, n = size(M)
    W = rand(m,r)
    H = rand(r,n)
    return W, H
end

"""
    initlambda(r, lambda)

Given a number of lines r and a matrix 'lambda' of size r*1, if 'lambda' is a number,
it returns a matrice of size r*1 full of 'lambda', if 'lambda' is a matrix,
it returns lambda.
"""
function initlambda(r, lambda)
    if length(lambda) == 1
        return ones(r, 1) * lambda
    else
        return lambda
    end
end # function


"""
    scale!(W, H)

Given a matrix W of size m*r and a matrix H of size r*n, scale W and H so that
the values of the lines of H are between 0 and 1 and keep the proportions. The
columns of W are update too in order to keep the result of W*H unchanged.
"""
function scale!(W, H)
    for k in 1:size(W, 2)
        maxHk = maximum( H[k,1:end])
        if maxHk != 0
            H[k,1:end] ./= maxHk
            W[1:end, k] .*= maxHk
        end # if
    end
end # function

end  # module
