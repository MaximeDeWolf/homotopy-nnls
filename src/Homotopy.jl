module Homotopy
export find_breakpoints

using LinearAlgebra: diag

include("../src/NNLS.jl")


"""
    find_breakpoints(A, b)

Given a matrix A of size m*r  and a matrix b of size m*1,
such that with want to minimize ||Ax - b||_2 + lambda||x||_1. This function finds all the lambda
values that are breakpoints and their support. A breakpoint is a lambda value that
causes a 0 entry of x to become non-zero or the inverse. The support of a breakpoint
is the indices of x that are non-zero for this particular breakpoint. For each of
these supports, we compute the corresponding x. Then the list
of tuples (x, length of support) is returned.
"""
function find_breakpoints(A, b)
    epsilon = 1e-10
    H = A' * A
    l = A' * b
    j = argmax(l)
    K = [j]; x = build_x(inv(H[K, K]) * l[K], K, size(A, 2))
    x_and_support_size = [(x, length(K))]
    current_lambda = l[j]
    while current_lambda > epsilon
        next_lambda, K = compute_next_lambda(H, l, K, current_lambda)
        current_lambda = next_lambda
        # Check if we need to compute the support for the next lambda
        if next_lambda > epsilon
            x = build_x(inv(H[K, K]) * l[K], K, size(A, 2))
            if minimum(x) < 0
                # If the solution contains negative value, the non-zero values are
                # re-computed thanks to NNLS
                K = nnls_x!(x, K, A, b)
            end # if
            push!(x_and_support_size, (x, length(K)))
        end # if
    end # while
    return x_and_support_size
end # function

"""
    nnls_x!(x, K, A, b)

Given a vector x of size r*1, K the current support, A a matrix of size m*r and
b a matrix of size m*1, compute a new vector x so that all non-zero value of x
are replaced by new values such that "A * x =~ b" (these are computed with a nnls
algorithm). This function also returns the new support of the vector x.
"""
function nnls_x!(x, K, A, b)
    x[K] .= NNLS.nnls(A[:,K], b)
    new_K = findall(!iszero, x[:, 1])
    return new_K
end # function

"""
    build_x(aK, K, x_size)

Given aK a list of float and K a list of indices such that aK and K have the same
size, retrieve x, a matrix of size x_size * 1 such that its elements located at
the indices in K have the same values as the ones in aK. The others elements of
x are 0.
"""
function build_x(aK, K, x_size)
    x = zeros(x_size, 1)
    x[K] = aK
    return x
end # function

"""
    compute_condition_variables(H, l, K)

Given a matrix H of size r*r, a matrix l of size r*1 and array K that represents
the current support, compute the variables aK, bK, cK and dK that are used to
compute the optimality conditions.
"""
function compute_condition_variables(H, l, K)
    HKK = inv(H[K, K])
    e1 = ones(length(K), 1)
    K_comp = compute_K_complement(K, size(H, 1))
    e2 = ones(length(K_comp), 1)
    HKcK = H[K_comp, K]

    aK = HKK * l[K]
    bK = HKK * e1
    cK = HKcK * aK - l[K_comp]
    dK = HKcK * bK - e2
    return aK, bK, cK, dK
end # function



"""
    compute_next_lambda(H, l, K, current_lambda)

Given a matrix H of size r*r, a matrix l of size r*1, an array K that represents
the current support and the current value of lambda, computes the next breakpoint
by computing C1 and C2: the 2 optimality conditions.
If 'max_lambda_C1' == 'max_lambda_C2' then, 'max_lambda_C1' is returned.
"""
function compute_next_lambda(H, l, K, current_lambda)
    aK, bK, cK, dK = compute_condition_variables(H, l, K)
    max_lambda_C1, K_C1 = find_max_C1_lambda(aK, bK, K, current_lambda)
    max_lambda_C2, K_C2 = find_max_C2_lambda(cK, dK, K, size(H, 1), current_lambda)
    if max_lambda_C1 >= max_lambda_C2
        return max_lambda_C1, K_C1
    else
        return max_lambda_C2, K_C2
    end # if
end # function


"""
    find_max_lambda(aK, bK, K, current_lambda)

Given aK and bK, the 2 variable to compute an optimality condition,  K the
current support and the current value of lambda, find i such as aK[i]/bK[i]
is maximized  and less or equal to the current_lambda. Then it returns its value
and i.
"""
function find_max_lambda(aK, bK, K, current_lambda)
    max_lambda = -Inf
    max_K_index = 1
    for k in 1:length(bK)
        if bK[k] < 0
            lambda = aK[k] / bK[k]
            if current_lambda >= lambda
                #if 2 indices have the same lambda, the smaller index is taken
                if (lambda == max_lambda && K[k] < K[max_K_index]) || lambda > max_lambda
                    max_lambda = lambda
                    max_K_index = k
                end # if
            end #if
        end # if
    end # for
    return max_lambda, max_K_index
end # function

"""
    find_max_C1_lambda(H, l, K, current_lambda)

Given a matrix H of size r*r, a matrix l of size r*1, an array K that represents
the current support and the current value of lambda, compute the potential next
breakpoint and its support thanks to the first optimality condition.
If 2 columns have the same lambda and this lambda is the maximum,
the result of the first columns encountered is returned.
"""
function find_max_C1_lambda(aK, bK, K, current_lambda)
    max_lambda, max_K_index = find_max_lambda(aK, bK, K, current_lambda)
    new_K = copy(K)
    filter!(e -> e != K[max_K_index], new_K)
    return max_lambda, new_K
end # function


"""
    find_max_C2_lambda(H, l, K, current_lambda)

Given a matrix H of size r*r, a matrix l of size r*1, an array K that represents
the current support and the current value of lambda, compute the potential next
breakpoint and its support thanks to the second optimality condition.
If 2 columns have the same lambda and this lambda is the maximum,
the result of the first columns encountered is returned.
"""
function find_max_C2_lambda(cK, dK, K, size_H, current_lambda)
    K_comp = compute_K_complement(K, size_H)
    max_lambda, max_K_index = find_max_lambda(cK, dK, K_comp, current_lambda)
    new_K = copy(K)
    if length(K_comp) > 0
        append!(new_K, K_comp[max_K_index])
    end #if
    return max_lambda, new_K
end # function

"""
    compute_K_complement(K, r)

Given the current support K and r, the maximal length of k , compute the
complement of K, which a list of indices that does not appear in K and such that
length(K) + length(complement of K) = r.
"""
function compute_K_complement(K, r)
    K_comp = [1:r;]
    filter!(e -> !(e in K), K_comp)
    return K_comp
end # function

end # module
