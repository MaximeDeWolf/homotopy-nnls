module FPG3
export update_W!, update_H!

using LinearAlgebra


"""
update W by col
"""
function update_W!(M, W, H; alpha0=0.05, num_iter=500, delta=1e-6, lambda=nothing)
    update_H!(M', H', W', alpha0=alpha0, num_iter=num_iter, delta=delta)
end


"""
update H by row
"""
function update_H!(M, W, H; alpha0=0.05, num_iter=500, delta=1e-6, lambda=nothing)
    # Init
    WtW = W'*W
    WtM = W'*M
    L = norm(WtW)
    alphas = Array{Float64}(undef, num_iter)
    alphas[1] = alpha0
    betas = Array{Float64}(undef, num_iter)
    simplex_row_proj!(H)
    Y = copy(H)
    # Loop
    i = 1
    eps0 = 0
    eps = 1
    while i < num_iter && eps >= delta*eps0
        old_H = copy(H)
        alphas[i+1] = ( sqrt(alphas[i]^4 + 4*alphas[i]^2 ) - alphas[i]^2) / (2)
        betas[i] = alphas[i]*(1-alphas[i])/(alphas[i]^2+alphas[i+1])
        # Projection step
        H .= Y - (WtW*Y-WtM) ./ L
        simplex_row_proj!(H)
        # `Optimal' linear combination of iterates
        Y .= H + betas[i] * (H - old_H)
        if i == 1
            eps0 = norm(H - old_H)
        end
        eps = norm(H - old_H)
        i += 1
    end
end # end of function update_H!


"""
projects rows of matrix H onto the unit simplex
"""
function simplex_row_proj!(H)
    for hi in eachrow(H)
        simplex_proj!(hi)
    end
end

"""
projects cols of matrix H onto the unit simplex
"""
function simplex_col_proj!(H)
    for hj in eachcol(H)
        simplex_proj!(hj)
    end
end

"""
projects vector x onto the unit simplex
"""
function simplex_proj!(x)
    n = length(x)
    xget = false

    idx = sortperm(x, rev=true)
    tsum = 0.0

    for i = 1:n-1
        tsum += x[idx[i]]
        tmax = (tsum - 1)/i
        if tmax >= x[idx[i+1]]
            xget = true
            break
        end
    end

    if !xget
        tmax = (tsum + x[idx[n]] - 1) / n
    end

    for i = 1:n
        x[i] = max(x[i] - tmax, 0)
    end

end

end # end of module FPG3
