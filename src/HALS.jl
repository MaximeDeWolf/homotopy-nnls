module HALS
export update_W!, update_H!

"""
    update_H!(M, W, H; lambda, num_iter=1)

Call 'update_W!(M', H', W'; lambda, num_iter=1)' in order to update H.
"""
function update_H!(M, W, H; lambda, num_iter=1)
    update_W!(M', H', W', lambda=lambda, num_iter=num_iter)
end # function

"""
    update_W!(M, W, H; lambda, num_iter=1)

Given a matrix M of size m*n, a matrix W of size m*r W and H a matrix of size
r*n, updates the matrix W in order to minimize the difference between M and
W * H according to the Frobenius norm. The matix H is frozen. The lambda is a
matrix of size r*1 which allows to control the L1 regularization. If lambda is
not provided, a matrix full of 0 of size r*1 is used instead. That means that
by default the L1 regularization is not used.
"""
function update_W!(M, W, H; lambda, num_iter=1)
    # We compute static values outside the loop to spare some time
    HHt = H * H'
    MHt = M * H'
    for i in 1:num_iter
        for column in 1:size(W, 2)
            # for each element of "W[1:end, column]", assign the equivalent element
            # of "gradient(column, MHt, W, HHt, H)" if it is positive otherwise,
            # assign 0.
            W[1:end, column] .= max.(0, gradient(column, MHt, W, HHt, lambda=lambda[column]))
        end
    end # for
end # function

"""
    gradient(l, MHt, W, HHt)

Given l the index of the current column being processed, MHt an m*r matrix,
W an m*r matrix and HHt an r*r matrix, computes the updated value of column l
of the matrix W according to the Frobenius norm.
"""
function gradient(l, MHt, W, HHt; lambda=0)
    grad = MHt[1:end, l]
    for k in 1:size(W, 2)
        if k != l
            grad -= W[1:end, k] * HHt[k, l]
        end
    end # for
    grad .-= lambda * sum(W[1:end, l])
    if  HHt[l, l] != 0
        return grad / HHt[l, l]
    else
        return grad
    end
end # function

end #module
