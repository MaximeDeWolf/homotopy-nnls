# Julia packages
using LinearAlgebra

# Our NMF module
include("../src/NMF.jl")
include("../src/HALS.jl")
include("../src/FastProjectedGradient.jl")
include("../src/Homotopy_NNLS.jl")


"""
    make_table_entry(r, k, n, static_time, static_error, budget_time, budget_error)

Given three integer r, k and n and four float static_time, static_error,
budget_time and budget_error put these data into a string that represents a latex
table entry.
"""
function make_table_entry(r, k, n, static_time, static_error, budget_time, budget_error)
    """
    $n & $r & $k & $(round(static_error*100, digits=2)) & $(round(static_time, digits=2)) & $(round(budget_error*100, digits=2)) & $(round(budget_time, digits=2))\\\\
    \\hline\n"""
end # function

"""
    apply_NMF(r, k, n)

Given three integer r, k and n, compute NMF using the static and the budget
algorithm on a matrix M of size 100*n randomly generated. The matrix W and H generated
are respectively of size 100*r and r*n and are used to compute the reconstruction
error of both algorithm.This function return the execution time and the reconstruction
error of the static algorithm and also he execution time and the reconstruction
error of the budget algorithm.
"""
function apply_NMF(r, k, n)
    trueW = rand(100, r)
    trueH = rand(r, n)
    M = trueW * trueH

    initW, initH = NMF.random_initialization(M, r)
    f = (M, r) -> return deepcopy(initW), deepcopy(initH)
    # static algorithm
    select_function = (M, W, sol) -> HNNLS.select_solution_of_size(M, W, sol, k)
    update! = (M, W, H; lambda) -> HNNLS.update_H!(M, W, H, lambda=lambda, selection_function=select_function)
    t_static = @elapsed W, H =  NMF.nmf(M, r, update_H! = update!, update_W! = HALS.update_W!, max_iter=60, initialization=f)
    static_error = norm(M - W * H) / norm(M)
    # static algorithm
    select_function = (M, W, sol) -> HNNLS.select_in_budget(M, W, sol, k*n, use_limit=false)
    update! = (M, W, H; lambda) -> HNNLS.update_H!(M, W, H, lambda=lambda, selection_function=select_function)
    t_budget = @elapsed W, H =  NMF.nmf(M, r, update_H! = update!, update_W! = HALS.update_W!, max_iter=60, initialization=f)
    budget_error = norm(M - W * H) / norm(M)
    return t_static, static_error, t_budget, budget_error
end # function

"""
    compare_dimension(r_list, k_step, n_start, n_end, n_step, file; erase_save=true)

Given r_list a list of integer, 4 integer k_step, n_start, n_end and n_step and
filename "file", launch the static and the budget algorithm on random data of size
100*n with variation of r in the range "r_list", of k from 3 to the current value
of r by step of "k_step" and of n from "n_start" to "n_end" by step of "n_step".
The results are stored in the file "file". If earse_save is true, the content
of the "file" file will be replaced by the newly generated data. Otherwise, the
new data is concatenated at the end of the file.
"""
function compare_dimension(r_list, k_step, n_start, n_end, n_step, file; erase_save=true)
    # force Julia to "compile" the code and avoid the overhead of time
    apply_NMF(1, 1, 100)
    if erase_save
        open(file, "w") do f
        end
    end # if
    for r in r_list
        k = 3
        while k < r
            n = n_start
            while n <= n_end
                t_static, static_error, t_budget, budget_error = apply_NMF(r, k, n)
                open(file, "a") do f
                    write(f, make_table_entry(r, k, n, t_static, static_error, t_budget, budget_error))
                end
                n += n_step
            end # while
            k += k_step
        end # while
    end # for
end # function

compare_dimension(6:10, 3, 1000, 4000, 1000, "experiments/output/dimension_comparison_table.tex"; erase_save=true)
