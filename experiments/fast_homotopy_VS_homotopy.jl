# Julia packages
using LinearAlgebra
using MAT # to read .mat files
using Plots
# select PyPlot as plot backend
Plots.pyplot()

# Our NMF module
include("../src/NMF.jl")
include("../src/FastProjectedGradient.jl")
include("../src/FastCoordDesc.jl")
include("../src/HALS.jl")
include("../src/Homotopy_NNLS.jl")
include("util/displayabundancemap.jl")
include("util/sub_image.jl")
include("util/colsparsity.jl")


"""
    fast_homotopy_VS_homotopy(M, r, image_width, image_height, v_grid, h_grid)

Given M a matrix of size m*n and r an integer, launch the NMF algorithm twice on
a 307*307 image: the Urban image. The first time with the static homotopy
algorithm with fast homotopy for the update of H and the second time with the static homotopy
algorithm with classic homotopy. For each one, the reconstrucion error and the execution
time are printed to the terminal, the abundance map is also created.
The four integer image_width, image_height, v_grid and h_grid are parameter for
the creation of the abundance maps. image_width and image_height are respectively
the width and the height of each image of the abundance maps. v_grid and h_grid
represents the layout of the abundance map, this means that the images of the
abundace maps are displayed in a gris of v_grid*h_grid.
"""
function fast_homotopy_VS_homotopy(M, r, image_width, image_height, v_grid, h_grid; init)
    # homotopy with static algorithm
    select_function(M, W, sol) =  HNNLS.select_solution_of_size(M, W, sol, 2)
    launch_NMF_fast(M, r, select_function, "Algorithme statique: homotopie \"rapide\"", image_height, image_width, v_grid, h_grid, "experiments/output/fast_homotopy_static.png", init)
    launch_NMF(M, r, select_function, "Algorithme statique: homotopie \"classique\"", image_height, image_width, v_grid, h_grid, "experiments/output/homotopy_static.png", init)
end # function

"""
    HALS_VS_HALS_L1(M, r, image_width, image_height, v_grid, h_grid)

Given M a matrix of size m*n and r an integer, launch the NMF algorithm twice on
a 307*307 image: the Urban image. The first time with the HALS
algorithm for the update of H and the second time with the HALS
algorithm penalized with the L1 norm. For each one, the reconstrucion error and the execution
time are printed to the terminal, the abundance map is also created.
The four integer image_width, image_height, v_grid and h_grid are parameter for
the creation of the abundance maps. image_width and image_height are respectively
the width and the height of each image of the abundance maps. v_grid and h_grid
represents the layout of the abundance map, this means that the images of the
abundace maps are displayed in a gris of v_grid*h_grid.
"""
function HALS_VS_HALS_L1(M, r, image_width, image_height, v_grid, h_grid; init)
    # Force Julia to compile in order to avoid the time overhead
    NMF.nmf(rand(5, 6),2, update_H! = HALS.update_H!, update_W! = FPG.update_W!, max_iter=60)
    NMF.nmf(rand(5, 6),2, update_H! = FastCoordDesc.update_H!, update_W! = HALS.update_W!, max_iter=60, lambda=ones(6, 1))

    t = @elapsed W, H = NMF.nmf(M,r, update_H! = HALS.update_H!, update_W! = FPG.update_W!, max_iter=60, initialization=init)
    println("Algorithme HALS")
    println("\tErreur de reconstruction: $(norm(M - W * H) / norm(M))")
    println("\tTemps d'exécution: $t")
    displayabundancemap(H, image_height, image_width, v_grid, h_grid, "experiments/output/HALS.png", bw=false)

    lambda = [0.; 0.00001; 0.0007; 0.; 0.00001; 0.0001]
    t = @elapsed W, H = NMF.nmf(M,r, update_H! = HALS.update_H!, update_W! = FPG.update_W!, max_iter=60, lambda=lambda, initialization=init)
    println("Algorithme HALS pénalisé")
    println("\tErreur de reconstruction: $(norm(M - W * H) / norm(M))")
    println("\tTemps d'exécution: $t")
    displayabundancemap(H, image_height, image_width, v_grid, h_grid, "experiments/output/HALS_L1.png", bw=false)
end # function

"""
    launch_NMF_fast(M, r, select_function, headline, map_nrows, map_ncols, filename)

Given M a matrix of size m*n, an integer r, select_function a ready to use homotopy,
function, a string headline, two integer map_nrows and map_ncols and filename,
launch the NMF on M and r and use the select_function in order to update H. Then,
the reconstruction error and the execution time are printed in the terminal. Finally,
the abudance map of size map_nrows*map_ncols is created as the "filename" filename.
v_grid and h_grid represents the layout of the abundance map, this means that
the images of the abundace maps are displayed in a gris of v_grid*h_grid.
"""
function launch_NMF_fast(M, r, select_function, headline, map_nrows, map_ncols, v_grid, h_grid, filename, init)
    # Force Julia to compile in order to avoid the time overhead
    NNLS_fast_homotopy(rand(5, 6), 2, select_function)
    t = @elapsed W, H = NNLS_fast_homotopy(M, r, select_function, init)
    println(headline)
    println("\tErreur de reconstruction: $(norm(M - W * H) / norm(M))")
    println("\tTemps d'exécution: $t")
    displayabundancemap(H, map_nrows, map_ncols, v_grid, h_grid, filename, bw=false)

end # function

"""
    launch_NMF(M, r, select_function, headline, map_nrows, map_ncols, filename)

Given M a matrix of size m*n, an integer r, select_function a ready to use homotopy,
function, a string headline, two integer map_nrows and map_ncols and filename,
launch the NMF on M and r and use the select_function in order to update H. Then,
the reconstruction error and the execution time are printed in the terminal. Finally,
the abudance map of size map_nrows*map_ncols is created as the "filename" filename.
v_grid and h_grid represents the layout of the abundance map, this means that
the images of the abundace maps are displayed in a gris of v_grid*h_grid.
"""
function launch_NMF(M, r, select_function, headline, map_nrows, map_ncols, v_grid, h_grid, filename, init)
    # Force Julia to compile in order to avoid the time overhead
    NNLS_homotopy(rand(5, 6), 2, select_function)
    t = @elapsed W, H = NNLS_homotopy(M, r, select_function, init)
    println(headline)
    println("\tErreur de reconstruction: $(norm(M - W * H) / norm(M))")
    println("\tTemps d'exécution: $t")
    displayabundancemap(H, map_nrows, map_ncols, v_grid, h_grid, filename, bw=false)

end # function

"""
    NNLS_fast_homotopy(M, r, select_function)

Given M a matrix of size m*n, r an integer and select_function a function that can
choose the best solution among a set, find W a matrix of size m*r and H a matrix
of size r*n such that M ≃ W*H. W and H are computed thanks to HALS but an homotopy
algorithm is used afterwards (for only 1 iteration). This homotopy algorithm
use the select_function in order to choose the best solution.
"""
function NNLS_fast_homotopy(M, r, select_function, init)
    W, H = NMF.nmf(M,r, update_H! = HALS.update_H!, update_W! = FPG.update_W!, max_iter=60, initialization=init)
    f = (x, y) -> return deepcopy(W), deepcopy(H)
    update!(M, W, H; lambda) = HNNLS.update_H!(M, W, H, lambda=lambda, selection_function=select_function)
    W, H = NMF.nmf(M,r, update_H! = update!, update_W! = FPG.update_W!, initialization=f, max_iter=10)
    return W, H
end # function

"""
    NNLS_homotopy(M, r, select_function)

Given M a matrix of size m*n, r an integer and select_function a function that can
choose the best solution among a set, find W a matrix of size m*r and H a matrix
of size r*n such that M ≃ W*H. W and H are computed thanks to HALS but an homotopy
algorithm is used afterwards (for only 1 iteration). This homotopy algorithm
use the select_function in order to choose the best solution.
"""
function NNLS_homotopy(M, r, select_function, init)
    update!(M, W, H; lambda) = HNNLS.update_H!(M, W, H, lambda=lambda, selection_function=select_function)
    W, H = NMF.nmf(M,r, update_H! = update!, update_W! = FPG.update_W!, max_iter=60, initialization=init)
    return W, H
end # function


# Initialize parameters
r = 6
datafile = "data/Urban.mat"
varname = "A" # name of the variable of the file corresponding to input matrix

# Read matrix
data = matopen(datafile)
# M: matrix of size 162 * 94249
# original image has size of 307 * 307
M = Array(read(data, varname)')
image_width = 307
image_height = 307
initW, initH = NMF.random_initialization(M, r)
f = (M, r) -> return deepcopy(initW), deepcopy(initH)
# fast_homotopy_VS_homotopy(M, r, image_width, image_height, 2, 3; init=f)
HALS_VS_HALS_L1(M, r, image_width, image_height, 2, 3; init=f)
