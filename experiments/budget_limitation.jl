include("../src/NMF.jl")
include("../src/FastProjectedGradient.jl")
include("../src/HALS.jl")
include("../src/Homotopy_NNLS.jl")

using Plots
# select PyPlot as plot backend
Plots.pyplot()

"""
    test_budget_limitation(start_n, max_n, increment, file; erase_save=true)

Given an integer start_n, an integer max_n, an integer increment, a name of file "file"
and erase_save an optinal boolean, launch only one iteration of NMF using FPG for
the W update and the homotopy budget algorithm for the H update and store the time to terminate
this iteration in the "file" file. This operation is repeated for each number of M columns from
start_n to max_n by step of increment. The current number of M column and the time
needed by the homotopy budget algorithm in order to process them are written in
the "file" file at each iteration of the loop. You can thus conduct this experiment
in several times without losing the results. If earse_save is true, the content
of the "file" file will be replaced by the newly generated data. Otherwise, the
new data is concatenated at the end of the file.
"""
function test_budget_limitation(start_n, max_n, increment, file; erase_save=true)
    m = 10
    n = start_n
    r = 4
    M = rand(m, max_n)
    if erase_save
        open(file, "w") do f
        end
    end # if
    while n <= max_n
        select_function(M, W, sol) = HNNLS.select_in_budget(M, W, sol, 2*n, use_limit=false)
        update!(M, W, H; lambda) = HNNLS.update_H!(M, W, H, lambda=lambda, selection_function=select_function)
        t = @elapsed NMF.nmf(M[:, 1:n], r, update_H! = update!, update_W! = FPG.update_W!, max_iter=1)
        println(t)
        open(file, "a") do f
            write(f, "$n $t\n")
        end
        n += increment
    end # while
end # function

"""
    test_static_limitation(start_n, max_n, increment, file; erase_save=true)

Given an integer start_n, an integer max_n, an integer increment, a name of file "file"
and erase_save an optinal boolean, launch only one iteration of NMF using FPG for
the W update and the homotopy static algorithm for the H update and store the time to terminate
this iteration in the "file" file. This operation is repeated for each number of M columns from
start_n to max_n by step of increment. The current number of M column and the time
needed by the homotopy budget algorithm in order to process them are written in
the "file" file at each iteration of the loop. You can thus conduct this experiment
in several times without losing the results. If earse_save is true, the content
of the "file" file will be replaced by the newly generated data. Otherwise, the
new data is concatenated at the end of the file.
"""
function test_static_limitation(start_n, max_n, increment, file; erase_save=true)
    m = 10
    n = start_n
    r = 4
    M = rand(m, max_n)
    if erase_save
        open(file, "w") do f
        end
    end # if
    # avoid to have the first time mesure with overhead
    select_function(M, W, sol) = HNNLS.select_solution_of_size(M, W, sol, 2)
    update!(M, W, H; lambda) = HNNLS.update_H!(M, W, H, lambda=lambda, selection_function=select_function)
    t = @elapsed NMF.nmf(M[:, 1:n], r, update_H! = update!, update_W! = FPG.update_W!, max_iter=1)
    while n <= max_n
        t = @elapsed NMF.nmf(M[:, 1:n], r, update_H! = update!, update_W! = FPG.update_W!, max_iter=1)
        open(file, "a") do f
            write(f, "$n $t\n")
        end
        n += increment
    end # while
end # function

"""
    test_HALS_limitation(start_n, max_n, increment, file; erase_save=true)

Given an integer start_n, an integer max_n, an integer increment, a name of file "file"
and erase_save an optinal boolean, launch only one iteration of NMF using FPG for
the W update and the homotopy static algorithm for the H update and store the time to terminate
this iteration in the "file" file. This operation is repeated for each number of M columns from
start_n to max_n by step of increment. The current number of M column and the time
needed by the homotopy budget algorithm in order to process them are written in
the "file" file at each iteration of the loop. You can thus conduct this experiment
in several times without losing the results. If earse_save is true, the content
of the "file" file will be replaced by the newly generated data. Otherwise, the
new data is concatenated at the end of the file.
"""
function test_HALS_limitation(start_n, max_n, increment, file; erase_save=true)
    m = 10
    n = start_n
    r = 4
    M = rand(m, max_n)
    if erase_save
        open(file, "w") do f
        end
    end # if
    # avoid to have the first time mesure with overhead
    t = @elapsed NMF.nmf(M[:, 1:n], r, update_H! = HALS.update_H!, update_W! = FPG.update_W!, max_iter=1)
    while n <= max_n
        t = @elapsed NMF.nmf(M[:, 1:n], r, update_H! = HALS.update_H!, update_W! = FPG.update_W!, max_iter=1)
        open(file, "a") do f
            write(f, "$n $t\n")
        end
        n += increment
    end # while
end # function

"""
    draw_budget_plot(datafile, plotfile, plot_label)

Make a plot based on the data stored in the datafile and save it in the plotfile.
"""
function draw_budget_plot(datafile, plotfile, plot_label)
    X, Y = parse_file(datafile)
    plot = Plots.plot(X, Y, xlabel = "Nombre de colonnes", ylabel = "Temps en secondes", label=plot_label)
    Plots.savefig(plot, plotfile)
end # function

"""
    parse_file(datafile)

Parse the data stored in the "datafile" filename composed of n lines of data.
Each line of this file must contain 2 floats separated by a space. All the first
floats are stored in a matrix X of size(n, 1) and all the second floats are stored
in a matrix Y of size(n, 1). This function then returned X and Y.
"""
function parse_file(datafile)
    X = nothing
    Y = nothing
    open(datafile, "r") do f
        # mark the file at the current position
        mark(f)
        # read all the file and count the lines, the current position of the
        # reader is thus the end of the file
        line_count = countlines(f)
        # reset the file reader to the mark
        reset(f)
        X = zeros(line_count, 1)
        Y = zeros(line_count, 1)
        current_index = 1
        for line in eachline(f)
            values = split(line)
            X[current_index, 1] = parse(Float64, values[1])
            Y[current_index, 1] = parse(Float64, values[2])
            current_index += 1
        end
    end # close the file
    return X, Y
end # function

"""
    draw_plot(static_datafile, HALS_datafile, plotfile, plot_label)

Make a plot using the data stored in the files static_datafile and HALS_datafile.
These two files will produce 2 two lines on the same graph. The graph is then saved
in the plotfile.
"""
function draw_plot(static_datafile, HALS_datafile, plotfile, plot_label)
    X1, Y1 = parse_file(static_datafile)
    X2, Y2 = parse_file(HALS_datafile)
    Y = [Y1[:, 1] Y2[:, 1]]
    plot = Plots.plot(X1, Y, xlabel = "Nombre de colonnes", ylabel = "Temps en secondes", label=plot_label)
    Plots.savefig(plot, plotfile)
end # function

test_budget_limitation(10, 100010, 1000, "experiments/output/limit_budget_results.txt")
test_static_limitation(10, 100010, 1000, "experiments/output/limit_static_results.txt")
test_HALS_limitation(10, 100010, 1000, "experiments/output/limit_HALS_results.txt")
draw_budget_plot("experiments/output/limit_budget_results.txt", "experiments/output/limit_budget.png", "Algorithme de budget")
draw_plot("experiments/output/limit_static_results.txt", "experiments/output/limit_HALS_results.txt", "experiments/output/limit_static_HALS.png", ["Algirithme staique" "HALS"])
