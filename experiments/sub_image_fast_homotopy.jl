# Julia packages
using LinearAlgebra
using MAT # to read .mat files
using Plots
# select PyPlot as plot backend
Plots.pyplot()

# Our NMF module
include("../src/NMF.jl")
include("../src/FastProjectedGradient.jl")
include("../src/HALS.jl")
include("../src/Homotopy_NNLS.jl")
include("util/displayabundancemap.jl")
include("util/sub_image.jl")


"""
    sub_image_fast_homotopy(M, r)

Given M a matrix of size m*n and r an integer, launch the NMF algorithm twice on
a 80*80 sub-image of the Urban image. The first time with the static homotopy
algorithm for the update of H and the second time with the budget homotopy
algorithm. For each one, the reconstrucion error and the execution time are printed
to the terminal, the abundance map is also created.
"""
function sub_image_fast_homotopy(M, r, image_width, image_height, sub_width, sub_height, sub_posx, sub_posy)
    indexes = get_subimage_indexes(image_height, image_width, sub_width, sub_height, sub_posx, sub_posy)

    initW, initH = NMF.random_initialization(M[:, indexes], r)
    f = (M, r) -> return deepcopy(initW), deepcopy(initH)
    # homotopy with static algorithm
    select_function(M, W, sol) =  HNNLS.select_solution_of_size(M, W, sol, 2)
    launch_NMF(M[:, indexes], r, select_function, "Algorithme statique", sub_height, sub_width, "experiments/output/sub_image_fast_homotopy_static.png", f)

    # homotopy with budget algorithm
    select_function2(M, W, sol) =  HNNLS.select_in_budget(M, W, sol, 2*sub_width*sub_height, limit=1800)
    launch_NMF(M[:, indexes], r, select_function2, "Algorithme de budget", sub_height, sub_width, "experiments/output/sub_image_fast_homotopy_budget.png", f)
end # function

"""
    launch_NMF(M, r, select_function, headline, map_nrows, map_ncols, filename)

Given M a matrix of size m*n, an integer r, select_function a ready to use homotopy,
function, a string headline, two integer map_nrows and map_ncols and filename,
launch the NMF on M and r and use the select_function in order to update H. Then,
the reconstruction error and the execution time are printed in the terminal. Finally,
the abudance map of size map_nrows*map_ncols is created as the "filename" filename.
"""
function launch_NMF(M, r, select_function, headline, map_nrows, map_ncols, filename, init)
    NNLS_fast_homotopy(rand(5, 6), 2, select_function)
    t = @elapsed W, H = NNLS_fast_homotopy(M, r, select_function, init)
    println(headline)
    println("\tErreur de reconstruction: $(norm(M - W * H) / norm(M))")
    println("\tTemps d'exécution: $t")
    displayabundancemap(H, map_nrows, map_ncols, 2, 3, filename, bw=false)

end # function

"""
    NNLS_fast_homotopy(M, r, select_function)

Given M a matrix of size m*n, r an integer and select_function a function that can
choose the best solution among a set, find W a matrix of size m*r and H a matrix
of size r*n such that M ≃ W*H. W and H are computed thanks to HALS but an homotopy
algorithm is used afterwards (for only 1 iteration). This homotopy algorithm
use the select_function in order to choose the best solution.
"""
function NNLS_fast_homotopy(M, r, select_function, init)
    W, H = NMF.nmf(M,r, update_H! = HALS.update_H!, update_W! = FPG.update_W!, max_iter=60, initialization=init)
    f = (x, y) -> return deepcopy(W), deepcopy(H)
    update!(M, W, H; lambda) = HNNLS.update_H!(M, W, H, lambda=lambda, selection_function=select_function)
    W, H = NMF.nmf(M,r, update_H! = update!, update_W! = FPG.update_W!, initialization=f, max_iter=10)
    return W, H
end # function


# Initialize parameters
r = 6
datafile = "data/Urban.mat"
varname = "A" # name of the variable of the file corresponding to input matrix

# Read matrix
data = matopen(datafile)
# M: matrix of size 162 * 94249
# original image has size of 307 * 307
M = Array(read(data, varname)')
image_width = 307
image_height = 307
# get a sub-image of size 80*80 which is more or less centererd on the original one
sub_image_fast_homotopy(M, r, image_width, image_height, 80, 80, 110, 110)
