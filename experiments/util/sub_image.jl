"""
    get_subimage_indexes(image_height, image_width, sub_height, sub_width, sub_posx, sub_posy)

Given six integer image_height the height of the original image (in pixel), image_width
the width of the original image (in pixel), sub_height the height of the sub-image
(in pixel), sub_width the width of the sub-image (in pixel), sub_posx the x coordinate
where the sub-image begins on the original image and sub_posy the y coordinate
where the sub-image begins on the original image. Create a list of indexes such
that it allows to select the sub_image in the M matrix that represents the original
image.
"""
function get_subimage_indexes(image_height, image_width, sub_height, sub_width, sub_posx, sub_posy)
    indexes = []
    for i in 1:sub_height
        start_index = ((sub_posy+i-1)*image_width) + sub_posx
        append!(indexes, start_index:(start_index+sub_width-1))
    end # for
    return indexes
end # function
