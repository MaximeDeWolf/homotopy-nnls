using Random

"""
    rand_ksparse(m, n, k)

Generates a random matrix of size m*n with exactly k nonzero entries by column.
"""
function rand_ksparse(m, n, k)
    mat = zeros(m, n)
    for col in eachcol(mat)
        col .= rand_ksparse_vect(m, k)
    end
    return mat
end


"""
    rand_ksparse_vect(n, k)

Generates a random vector of size n with exactly k nonzero entries.
"""
function rand_ksparse_vect(n, k)
    k <= n || error("k is greater than n")
    vect = zeros(n)
    perm = randperm(n)[1:k]
    for i in perm
        vect[i] = rand()
    end
    return vect
end
