using Images

"""
    displayabundancemap(H, nrows, ncols, filename, bw)

Display the mixing matrix resulting from the unmixing of an hyperspectral image with NMF.
H is the mixing matrix, where every column is a pixel, and every row a material.
nrows and ncols are the number of pixels per row and per column of the image.
filename is the name of the file where to save the resulting image.
bw is an optinal boolean, if true the grayscale is inverted (can produce nicer pics), it is true by default.
"""
function displayabundancemap(H, nrows, ncols, filename; bw=true)
    if any(isnan, H)
        display(H)
    end # if
    # Get  dimensions and check them
    r, n = size(H)
    nrows*ncols == n || error("The dimensions of the image don't match with the parameters")

    # Init the output image with a white column
    imgarr = ones(nrows)
    # Loop on all materials
    for row in eachrow(H)
        max = maximum(row)
        if max != 0
            # Normalize so that values are in [0, 1]
            row ./= max
        end # if
        # Revert black and white if needed
        if bw
            row = ones(size(row)) - row
        end
        # Create image for one material
        img = reshape(row, nrows, ncols)
        # Append it to the output image
        imgarr = hcat(imgarr, img, ones(nrows))
    end
    # Save image
    save(filename, colorview(Gray, imgarr))
end

"""
    check_errors(H, nrows, ncols, v_grid, h_grid)

Given a matric H of size r*n and 4 integers nrows, ncols, v_grid and h_grid,
verifies that n = nrows*ncols and r = v_grid*h_grid. This throws an error if it
is not the case.
"""
function check_errors(H, nrows, ncols, v_grid, h_grid)
    if any(isnan, H)
        display(H)
    end # if
    # Get  dimensions and check them
    r, n = size(H)
    nrows*ncols == n || error("The dimensions of the image don't match with the parameters")
    h_grid*v_grid == r || error("The dimensions of the grid don't match with the image dimension")
end # function

"""
    create_row_image(row, nrows, ncols, bw)

Given row: a vector of length of size nrows*ncols, nrows: an integer, ncols:
an integer and bw: a boolean. Normalize the date of "row" then reshape it in
order to create a matrix of size nrows*ncols and return it. If bw is true,
the data of "row" are "inverted" such that row = 1 - row.
"""
function create_row_image(row, nrows, ncols, bw)
    max = maximum(row)
    if max != 0
        # Normalize so that values are in [0, 1]
        row ./= max
    end # if
    # Revert black and white if needed
    if bw
        row = ones(size(row)) - row
    end
    # Create image for one material
    img = reshape(row, nrows, ncols)
    return img
end # function

"""
    displayabundancemap(H, nrows, ncols, filename, bw)

Display the mixing matrix resulting from the unmixing of an hyperspectral image with NMF.
H is the mixing matrix, where every column is a pixel, and every row a material.
nrows and ncols are the number of pixels per row and per column of the image.
filename is the name of the file where to save the resulting image.
bw is an optinal boolean, if true the grayscale is inverted (can produce nicer pics), it is true by default.
"""
function displayabundancemap(H, nrows, ncols, v_grid, h_grid, filename; bw=true)
    check_errors(H, nrows, ncols, v_grid, h_grid)
    # Init the output image with a white column
    imgarr = ones(nrows * v_grid, ncols * h_grid)
    # Loop on all materials
    for i in 1:v_grid
        for j in 1:h_grid
            row_index = (i-1)*h_grid + j
            img = create_row_image(H[row_index, :], nrows, ncols, bw)

            aMap_row_range = (i-1)*nrows+1 : i*nrows
            aMap_col_range = (j-1)*ncols+1 : j*ncols
            imgarr[aMap_row_range, aMap_col_range] .= img
        end # for
    end # for
    # Save image
    save(filename, colorview(Gray, imgarr))
end
