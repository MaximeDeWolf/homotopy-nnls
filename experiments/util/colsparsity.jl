"""
    colsparsity(mat, epsilon)

Returns the average column-wise sparsity of the given matrix, where sparsity
is defined as the number of entries greater than epsilon.
By default epsilon=1e-8.
"""
function colsparsity(mat, epsilon=1e-8)
    return count(x -> x > epsilon, mat) / size(mat, 2)
end
