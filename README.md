# Homotopy NNLS

# Required packages

- LinearAlgebra

# Required for experiments

- Random
- MAT
- Images
- FileIO
- ImageMagick
- Plots

# Experiments

To run a test, launch Julia in the root folder, and execute `include("test/test.jl")`.
As `test.jl` is not a module, the variables defined during the test (such as W and H) are reachable by the user after execution.

To run a test on a real-life data set, execute `include("test/realdata_test.jl")`.
Real-life data sets are stored with [Git LFS](https://about.gitlab.com/blog/2017/01/30/getting-started-with-git-lfs-tutorial/), so before cloning this project you have to install Git LFS and initialize it with the command `git lfs install`.
